
// Пример AST для программы на Pascal
var AST = 
    {
        "type": "Program",
        "name": {
            "type": "Identifier",
            "name": "MyProgram"
        },
        "body": [
            {
                "type": "Call",
                "name": {
                    "type": "Identifier",
                    "name": "WriteLn"
                },
                "arguments": [
                    {
                        "type": "String",
                        "data": "Hello, World!"
                    }
                ]
            }
        ]
    };

/** 
program MyProgram;
begin
    WriteLn ('Hello, world!');
end.
*/

var  ast = require("../ast.js")
    , tpl = require("../template.js");

var e = new ast.Context();
ast.initDefaultMethods(e);
tpl.loadTemplate(e, 'print');

tpl.renderToFile(e, 'Program', AST, 'hello.pas');
