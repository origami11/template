
var tast = require("./ast.js")
    , path = require("path")
    , fs = require("fs")
    , P = require("./jsparse.js")
    , PP = require("./print.js")
    , Grammar = require("./grammar.js").grammar;

var encoding = "utf-8";
var print = console.log;

var dirList = ["./"];
var templateExtension = "stp";

function rawEncoding(text) { 
    return text; 
}

function printObject(o) {
    for(var i in o) {
        print(i, ":", o[i]);
    }
}

function setConverter(name) {
    encodingConverter = name;
}

function findFile(prefix, templateExtension) {
    for(var i = 0; i < dirList.length; i++) {
        var fname = dirList[i] + prefix + "." + templateExtension;
        if (path.existsSync(fname)) {
            return fname;
        }        
    }
    return null;
}

function Template(name, context) {
    this.fileName = name;
    this.data = fs.readFileSync(name, encoding);
    this.context = context;    
    this.parseAll();
}

Template.prototype.parseAll = function() {
    var ast = Grammar.parser(new P.ParseState(this.data, 0));
    // index
    if (!ast.result) {
        throw "\n" + ast.error.getMessage(this.data, this.fileName);
    }
    ast = ast.ast;

    this.context.addMethods(ast);

    for(var i = 0; i < ast.length; i++) {
        if (ast[i] instanceof tast.ASTUsing) {
            loadTemplate(this.context, ast[i].name);
        }
    }
}

function render(context, entry, data) {
    // substitute;
    var result = "";
    if (context.methods.hasOwnProperty(entry)) {
        result = context.methods[entry].evaluate(data, context);
    }

    return result;
}

function renderToString(context, entry, def) {
    var code = render(context, entry, def);
    var text = PP.pretty_print(code);
    return text;
}

function mkPath(/**Array*/ path0) {
	if (path0.constructor != Array) path0 = path0.split(/[\\\/]/);
	var make = "";
	for (var i = 0, l = path0.length; i < l; i++) {
		make += path0[i] + '/';
		if (! path.existsSync(make)) {
			fs.mkdirSync(make);
		}
	}
}

function renderToFile (context, entry, def, file, convert) {
    convert = convert || rawEncoding;
    var code = render(context, entry, def);
    var text = PP.pretty_print(code);
    var dir = path.dirname(file);
    mkPath(dir);
    fs.writeFileSync(file, convert(text));
}

function loadTemplate(context, templateFile) {
    var fname = findFile(templateFile, templateExtension);
    if (!fname) {
        throw "not found template '" + templateFile + "'";
    }
    var sample = new Template(fname, context);
    
    return sample;
}

exports.renderToString = renderToString;
exports.renderToFile = renderToFile;
exports.loadTemplate = loadTemplate;
exports.dirList = dirList;
exports.rawEncoding = rawEncoding;
