/* Граматика для шаблона */

var P = require("./jsparse.js")
    , ast = require("./ast.js")
    , PP = require("./print.js");

function printLines(ls) {
    for(var i = 0; i < ls.length; i++) {
        print(ls[i]);
    }
}

/* Смещение строки слева */
function getLeftOffset(line) {
//    print(line[0] instanceof ast.ASTString);
    if (line[0] instanceof ast.ASTString) {
        var s = line[0].text, pos = 0;
        if (pos >= s.length) {
            return Number.MAX_VALUE;
        }
        while (s.charAt(pos) == " ") {
            pos++;
            if (pos >= s.length) {
                return Number.MAX_VALUE;
            }
        }
        return pos;
    }
    return 0;
}

function removeOffset(line, offset) {
    if (line[0] instanceof ast.ASTString) {
        line[0].text = line[0].text.substr(offset);
    }
}

function getLineBreak(s) {
    if (s.indexOf("\r\n", 0) >= 0) return "\r\n";
    return "\n";
}

function getLines(r) {
    var lines = [[]], last = 0;
    for(var i = 0; i < r[1].length; i++) {
        var s = r[1][i];

        if (s instanceof ast.ASTString) {
            var JS_NEW_LINE = getLineBreak(s.text);
            var begin = 0;
            
            while ((index = s.text.indexOf(JS_NEW_LINE, begin)) >= 0) {
                lines[last].push(new ast.ASTString(s.text.substring(begin, index)), PP.NEWLINE); 
                last++;
                lines.push([]);
                begin = index + JS_NEW_LINE.length;
            }
            
            lines[last].push(new ast.ASTString(s.text.substr(begin)));
        } else {
            lines[last].push(PP.OFFSET_SAVE, s, PP.OFFSET_RESTORE);
        }        
    }
    return lines;
}

function compress(r0, r1) {
    var result = [r0];
    for(var i = 0; i < r1.length; i++) {
        result.push(r1[i][1]);
    }
    return result;
}

var g = {};
g.lparen = P.token('(');
g.rparen = P.token(')');

g.lbrace = P.token('{');
g.rbrace = P.token('}');

g.lbracket = P.token('[');
g.rbracket = P.token(']');

g.obra = P.token('<');
g.cbra = P.token('>');

g.eql = P.token('=');
g.neql = P.token('!=');
g.dots = P.token(':');
g.dot = P.token('.');

g.star = P.token('*');
g.comma = P.token(',');
g.tbegin = P.token('<<');
g.tend = P.token('>>');
g.hbegin = P.token('<%');
g.hend = P.token('%>');
g.newline = P.token('\n');
g.listKw = P.token('List');
g.hasKey = P.token('hasKey');
g.falseKw = P.token('$False');
g.isA = P.token('is');
g.number = P.action(P.rtoken('\\d+(\\.\\d)*', '<number>'), function (r) { return new ast.ASTNumber(parseFloat(r)); });
g.name = P.rtoken('[A-Za-z][\\w\\d_]*', '<ident>');

g.string = P.action(P.rtoken('"[^"]*"'), function (r) { return new ast.ASTString(r.substring(1, r.length - 1)); });

g.args = P.optional(P.action(P.wsequence(g.name, P.repeat0(P.wsequence(",", g.name))), function (r) {
    return compress(r[0], r[1]);
}));
//g.template = P.repeat0(P.choice(sequence(except(g.hbegin), g.hbegin, g.name, g.hend), not(g.tend)));
//g.template = P.repeat0(P.choice(not(g.tend), ));
g.tcontent = function(state) { return g.tcontent(state); }

g.part = P.action(P.repeat0(P.negate(P.choice(g.hbegin, g.tend))), function (r) { return new ast.ASTString(r.join("")); });

g.access = P.action(P.wsequence(g.name, P.repeat0(P.wsequence(".", g.name))), function (r) {
    return new ast.ASTAccess(compress(r[0], r[1]));
}); 

g.value = P.choice(g.access, g.string, g.number);

g.arguments = P.action(P.wsequence("(", g.value, P.repeat0(P.wsequence(",", g.value)), ")"), function (r) {
    return compress(r[1], r[2]);
});

g.expression = P.action(P.wsequence(
    g.access,
    P.repeat0(
        P.action(P.sequence(g.dots, g.name, P.optional(g.arguments), P.optional("*")), function (r) {
            return new ast.ASTCall(r[1], r[3] == '*', r[2]); 
        }))), function (r) {
        return new ast.ASTCode(r[0], r[1]);
    });

g.code = P.action(
    P.sequence(g.hbegin,
        P.optional(g.expression),
        g.hend), 
    function (r) { 
        return r[1] ? r[1] : new ast.ASTEmpty(); 
    });

g.tcontent = P.action(P.sequence(g.part, P.repeat0(P.sequence(g.code, g.part))), function (r) {
    var result = [r[0]];
    for(var i = 0; i < r[1].length; i++) {
        result.push(r[1][i][0]);
        result.push(r[1][i][1]);
    }
    return result;
});

g.tbody = P.action(P.sequence(g.tbegin, g.tcontent, g.tend), function (r) {

    // Разделить содержание на строки чтобы можно было их нармализировать    
    var lines = getLines(r);
    // Минимальное смещение
    var left = Number.MAX_VALUE;
    var i;
    for(i = 0; i < lines.length; i++) {
        var pos = getLeftOffset(lines[i]);
        left = Math.min(left, pos);
    }
    // Убираем смещение
    for(i = 0; i < lines.length; i++) {
        removeOffset(lines[i], left);
    }
    // Преобразуем в один массив
    var linear = [];
    for(i = 0; i < lines.length; i++) {
        linear = linear.concat(lines[i]);
    }
    // Убираем лишние строки до первого перевода строки и после последнего
    for(i = 0; i < linear.length; i++) {
        if (!(linear[i] instanceof ast.ASTString && PP.trim(linear[i].text).length == 0)) {
            if (linear[i] === PP.NEWLINE) i++;
            break;
        }
    }
    linear.splice(0, i);
    
    for(i = linear.length - 1; i >= 0; i--) {
        if (!(linear[i] instanceof ast.ASTString && PP.trim(linear[i].text).length == 0)) {
            if (linear[i] === PP.NEWLINE) i--;
            break;
        }
    }
    linear.splice(i + 1, linear.length - i - 1);

    return linear;
});

/* Правила сравнения */
g.operator = P.choice(
    P.action(g.eql, function (r) { return ast.OP_EQL; }), 
    P.action(g.neql, function (r) { return ast.OP_NOT_EQL; }),
    P.action(g.hasKey, function (r) { return ast.OP_HAS_KEY; }),
    P.action(g.isA, function (r) { return ast.OP_ISA; })
);

/* Обращение к свойству обьекта */
g.nameaccess = P.action(P.wsequence(g.name, P.repeat0(P.wsequence(".", g.name))), function (r) {
    return compress(r[0], r[1]);
}); 


g.literal = P.choice(g.name, P.action('#False', function (r) { return ast.False; }));

// g.name = string        
g.filter = P.action(P.wsequence(g.nameaccess, g.operator, g.literal), function (r) {
    return new ast.ASTPField(r[0], r[2], r[1]);
});

g.record = P.action(P.wsequence(g.filter, P.repeat0(P.wsequence(g.comma, g.filter))), function (r) {
    return new ast.ASTPRecord(compress(r[0], r[1]));
});

// Шаблон для сравнения со значением
g.pattern = P.action(P.wsequence("[", P.choice(g.record, g.literal), "]"), function (r) {
    return new ast.ASTPattern(r[1]);
});

g.margs = P.action(P.wsequence("(", g.args, ")"), function (r) {
    return r[1];
});

g.produce = P.action(P.wsequence(P.token('produce'), g.name, P.repeat0(P.wsequence(g.comma, g.name))), function (r) {
    return compress(r[1], r[2]);
});

g.methodInfo = P.action(P.wsequence(P.optional(g.pattern), P.optional(g.margs), P.optional(g.produce), g.tbody), function (r) {
    return new ast.MethodInfo(r[0], r[1], r[2], r[3]);
})

g.method = P.action(P.wsequence(g.name, g.eql, P.repeat1(g.methodInfo)), function (r) {
    return new ast.ASTMethod(r[0], r[2]);
});

g.using = P.action(P.wsequence(P.token('using'), g.name), function (r) {
    return new ast.ASTUsing(r[1]);
});


g.comment = P.repeat0(P.whitespace(P.sequence(P.token('#'), P.repeat0(P.negate(P.token('\n'))), P.token('\n'))));

g.methods = P.repeat0(P.action(P.sequence(g.comment, P.choice(g.using, g.method)), function (r) { return r[1]; }));

g.parser = P.action(P.wsequence(g.methods, P.end_p), function (r) { return r[0]; });


exports.grammar = g;
