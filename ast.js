// AST

var PP = require('./print.js');

var OP_EQL = 1;
var OP_NOT_EQL = 2;
var OP_HAS_KEY = 3;
var OP_ISA = 4;

function joinArray(a, separator) {
    var result = [];
    if (a.length >= 1) {
        for(var i = 0; i < a.length - 1; i++) {
            result.push(a[i], separator);
        }
        result.push(a[i]);
    }
    return result;
}

function extend(Child, Parent) {
	var F = function () {}
	F.prototype = Parent.prototype;
	Child.prototype = new F();
	Child.prototype.constructor = Child;
	Child.superClass = Parent.prototype;
}

function Listener() {
    this.list = [];
}

Listener.prototype.next = function (callback) {
    this.list.push(callback);
}

Listener.prototype.send = function () {
    for(var i = 0; i < this.list.length; i++) {
        this.list[i].apply(null, arguments);
    }
}

function Product(entry, data, file) {
    this.entry = entry;
    this.data = data;
    this.file = file;
}

function Context() {
    this.methods = {};
    /* Массив файлов для генерации */
    this.productions = [];
    this.completed = [];
    /* Карта для генерации файлов */
    this.map = {};
    /* Действие по завершении генерации */
    this.product = new Listener();
}

Context.prototype.setMap = function(map) {
    this.map = map;
}

Context.prototype.getMapFile = function (entry, data) {

    var filterName = {
        'lowercase': function (s) { return s.toLowerCase(); }, 
        'none': function (s) { return s;  }};

    if (!this.map.hasOwnProperty(entry)) {
        throw "no production for '" + entry + "'";
    }

    var pattern = this.map[entry];
    var found = pattern.match(/\{([\w_]+(\.[\w_]+)*(:[\w_]+)?)\}/g); 


    if (found) for(var i = 0; i < found.length; i++) {
        var name = found[i].substring(1, found[i].length - 1);
        var filter = filterName['none'];
        var pos = name.indexOf(":");
        if (pos >= 0) {
            filter = filterName[name.substring(pos + 1)];
            name = name.substring(0, pos);
        }
        name = name.split(".");

        if (hasPropertyA(data, name)) {
            pattern = pattern.replace(found[i], filter(getValueA(data, name)));
        } else {
            throw "can't substitute '" + name + "' in '"+ this.map[entry] + "'";
        }
    }
    return pattern;
}

Context.prototype.hasProduction = function (file) {
    var check = function (e) { return e.file == file  }
    return this.productions.some(check) || this.completed.some(check);
}

Context.prototype.addProduction = function (entry, data) {
    var file = this.getMapFile(entry, data)
    if (!this.hasProduction(file)) {
        this.productions.push(new Product(entry, data, file));
    }
}

Context.prototype.render = function (tpl) {
    while(this.productions.length > 0) {
        var p = this.productions.shift();
        this.product.send(this, p.entry, p.data, p.file);
        this.completed.push(p);
    }
}

Context.prototype.getMethod = function (name) {
    if (this.methods.hasOwnProperty(name)) {
        return this.methods[name];
    }
    throw 'undefined method "' + name + '"';   
}

Context.prototype.addMethod = function (name, fn) {
    this.methods[name] = fn;
}

Context.prototype.addMethods = function (ast) {
    for(var i = 0; i < ast.length; i++) {
        // Может быть несколько методов с одним с разными шаблонами или разным содержанимем
        // 1. Выполняем только с подходящим шаблоном, если несколько то группируем и разделяем нужным символом
        if (ast[i] instanceof ASTMethod) {
            var name = ast[i].name;
            // Проверяем есть ли методы 
            if (!this.methods.hasOwnProperty(name)) {
                this.methods[name] = new PolyMethod();
            }
            for(var j = 0; j < ast[i].list.length; j++) {
                ast[i].list[j].name = name;
                this.methods[name].addTemplate(ast[i].list[j]);
            }
        }
    }
}

function ASTAccess(list) {
    this.list = list;
}

ASTAccess.prototype.toString = function () {
    return this.list.join(".");
}

ASTAccess.prototype.evaluate = function (result, top, args) {
    var i = 0;
//    console.log('result', typeof result, this.list);
    if (this.list[0] == 'it') {
        i = 1;
    } else if (args.hasOwnProperty(this.list[0])) {
        i = 1;
        result = args[this.list[0]];
    }

    for(; i < this.list.length; i++) {
        if (hasProperty(result, this.list[i])) {
            result = getValue(result, this.list[i]);
        } else {
            throw 'no property "' + this.list + '"';
        }
    }
    return result;
}

function ASTPField(name, value, op) {
    this.name = name;
    this.value = value;
    this.op = op;
}

function ASTPRecord(fields) {
    this.fields = fields;
}


function ASTPattern(value) {
    this.value = value;
}

function hasPropertyA(valueA, nameA) {
    var result = valueA;
    for(var i = 0; i < nameA.length; i++) {
        if (hasProperty(result, nameA[i])) {
            result = getValue(result, nameA[i]);
        } else {
            return false;
        }
    }
    return true;
}

function getValueA(valueA, nameA) {
    var result = valueA;
    for(var i = 0; i < nameA.length; i++) {
        result = getValue(result, nameA[i]);
    }
    return result;
}

function typeOf(v, t) {
//    print(v.constructor, t);
    if (t == 'Array') {
        return (v.constructor == Array);
    }
    return false;
}

ASTPattern.prototype._match = function(valueA, valueB) {
    if (valueB == False) {
        return valueA === false;
    }
    return valueA == valueB;
}

// Сопоставление или фильтр, скорее всег фильтр => исправить парсер и сопоставление
ASTPattern.prototype.match = function(valueA, valueB) {
//    return value == this.value;
    if (valueB instanceof ASTPRecord && typeof valueA == 'object') {
        for(var i = 0; i < valueB.fields.length; i++) {

            var field = valueB.fields[i]; 
            // print(field); // field.name теперь массив
            // Совпадение
            if (field.op == OP_EQL) {
                if (!(hasPropertyA(valueA, field.name) && this._match(getValueA(valueA, field.name), field.value))) {
                    return false;
                }
            } 

            // Несовпадение
            if (field.op == OP_NOT_EQL) {
                if (!(hasPropertyA(valueA, field.name) && !this._match(getValueA(valueA, field.name), field.value))) {
                    return false;
                }
            } 

            // Несовпадение
            if (field.op == OP_ISA) {
                if (!(hasPropertyA(valueA, field.name) && typeOf(getValueA(valueA, field.name).value, field.value))) {
                    return false;
                }
            } 

            // Содержит ключ
            if (field.op == OP_HAS_KEY) {
                if (!(hasPropertyA(valueA, field.name) && getValueA(valueA, field.name).hasOwnProperty(field.value))) {
                    return false;
                }
            } 

        }
        return true;
    } 
    return this._match(valueA, valueB);
}

function PolyMethod() {
    this.templates = [];
}

PolyMethod.prototype.addTemplate = function (t) {
    this.templates.push(t);
}

PolyMethod.prototype.evaluate = function (value, top, args) {
    for(var i = 0; i < this.templates.length; i++) {
        var template = this.templates[i];
        if (!template.pattern || template.pattern.match(value, template.pattern.value)) {
            var r = template.evaluate(value, top, args);
            return r;
        } 
    }
    return ""; // Либо ошибку/предупреждение так-как нет не одного совпадения
}

// Нужно еще завести массив чтобы исключить зацикливание!!!
PolyMethod.prototype._abstract = function (top) {
    var result = [];
    for(var i = 0; i < this.templates.length; i++) {
        var template = this.templates[i];
        result.push(template._abstract(top));
    }
    return result;    
}

function MethodInfo(pattern, args, produce, code) {
    this.name = false;
    this.arguments = args || [];
    this.contents = code;
    this.pattern = pattern;
    this.produce = produce;
}

function ASTMethod(name, list) {
    this.name = name;
    this.list = list;
}

MethodInfo.prototype._abstract = function(top) {
    var result = [];

    var parts = this.contents;
    for(var k = 0; k < parts.length; k++) {
        if (parts[k] instanceof ASTCode) {
            result.push(parts[k]._abstract(top));
        }
    }

    return {name: this.name, next: result};
}

// FEXME Преименовать ctx  в me,self,it ???
MethodInfo.prototype.evaluate = function (ctx, top, args0) {
    if (this.produce) {
        for(var i = 0; i < this.produce.length; i++) {
            top.addProduction(this.produce[i], ctx);
        }
    }

    // Связать внутренних со значением
    var args = {};
    for(var i = 0; i < this.arguments.length; i++) {
        args[this.arguments[i]] = args0[i];
    }

    var parts = this.contents, part; 
    var cache = [], flush = 1;
    var line = [];
    for(var k = 0; k < parts.length; k++) {    
        if (typeof parts[k] == 'number' || parts[k] === PP.NEWLINE || parts[k] === PP.OFFSET_SAVE || parts[k] === PP.OFFSET_RESTORE) {
            part = parts[k];
        } else if(parts[k] instanceof ASTString) {
            part = parts[k].evaluate(ctx, top, args);
            if (!isEmpty(part)) {
                flush = 2;
            }
        } else {
            part = parts[k].evaluate(ctx, top, args);
            if (flush == 1 && isEmpty(part)) {
                flush = 0;
            } else if (!isEmpty(part)) {
                flush = 2;
            }
        }

        cache.push(part);
        if (parts[k] === PP.NEWLINE) {
            if (flush > 0) {
                line.push.apply(line, cache);
            }
            cache = [];
            flush = 1;
        }
    }

    if (flush > 0) {
        line.push.apply(line, cache);
    }    

    return new PP.CodeInfo(this.name, line, "method");
}

function isEmpty (s) {
    return (typeof s == 'undefined') || s === null || s === false || (typeof s == 'string' && PP.trim(s) == "") || (Array.isArray(s) && s.length == 0) || (s.constructor == PP.CodeInfo && s.isEmpty());
}

function ASTString(s) {
    this.text = s;
}

ASTString.prototype.evaluate = function (ctx, top, args) {
    return this.text;
}

ASTString.prototype.toString = function () {
    return this.text;    
}

function ASTNumber(s) {
    this.value = s;
}

ASTNumber.prototype.evaluate = function (ctx, top, args) {
    return this.value;
}

ASTNumber.prototype.toString = function () {
    return this.value;    
}

function ASTCall(name, map, arguments) {
    this.name = name;
    this.map = map;
    this.arguments = arguments || [];
}

function ASTEmpty() {
}

ASTEmpty.prototype.evaluate = function (ctx, top, args0) {
    return "";
}

function ASTCode(name, filter) {
    this.name = name;
    this.filter = filter;
}

ASTCode.prototype.process = function (filter, value, top, args) {
    var method = top.getMethod(filter);
    // Убрать сравнение с PolyMethod
    if (method instanceof PolyMethod) {
        return method.evaluate(value, top, args);
    } else {        
        return method(value, args);
    }
}

function hasProperty(o, name) {
    return o != null && (('get_' + name) in o || name in o);
}

function getValue(o, name) {
    if (('get_' + name) in o) {
        return o['get_' + name]();
    }
    return o[name];
}

ASTCall.prototype.evalArguments = function (ctx, top, args) {
    var result = [];
    for(var i = 0; i < this.arguments.length; i++) {
        result.push(this.arguments[i].evaluate(ctx, top, args));
    }
    return result;
}

/* Все таки нужно передавать окружение!! */
ASTCode.prototype._abstract = function (top) {
    var result = [];
    for(var i = 0; i < this.filter.length; i++) {
        var call = this.filter[i];
        var method = top.getMethod(call.name);
        if (method instanceof PolyMethod) {
            result.push(method._abstract(top));
        }
    }
    return result;
}

/* В контексте переменные, глобальные */
ASTCode.prototype.evaluate = function (ctx, top, args0) {
    /* Если значение массив то применить шаблон ко всем элементам массива */
    /* Сделать форматирование для шаблона удаление пустых строк, правильное количество */
    var value = this.name.evaluate(ctx, top, args0); //
//    if (value) {    
        for(var i = 0; i < this.filter.length; i++) {
            var call = this.filter[i];
            var args = call.evalArguments(ctx, top, args0);

            var j, s;
            if (call.map) {
                var result = [];
                if (Array.isArray(value)) {
                    for(j = 0; j < value.length; j++) {
                        s = this.process(call.name, value[j], top, args);
                        /* Не добавляем пустые данные в вывод */
                        if (!isEmpty(s)) {
                            result.push(s);
                        }
                    }
                    value = result;
                } else if (typeof value == 'object') {
                    for(j in value) {
                        s = this.process(call.name, value[j], top, args);
                        if (!isEmpty(s)) {
                            result.push(s);
                        }
                    }
                    value = result;
                }
            } else {
                //console.log(typeof value);
                value = this.process(call.name, value, top, args);
            }
        }
        return new PP.CodeInfo(this.name.toString(), value, "property");
//    }

//    return "";
}

function ASTUsing(name) {
    this.name = name;
}

ASTUsing.prototype.evaluate = function () {
}

function initDefaultMethods(context) {
    context.addMethod('camelcase', function (r) {
        return r[0].toUpperCase() + r.substring(1); 
    });
    
    /*context.addMethod('comment', function (r) {
        return r; 
    });*/
    
    context.addMethod('lowercase', function (r) {
        return r.toLowerCase(); 
    });
    
    context.addMethod('joins', function (r) {
        return joinArray(r, [",", PP.NEWLINE]);
    });

    context.addMethod('joinsp', function (r) {
        return joinArray(r, ", ");
    });

    context.addMethod('firstChar', function (r) {
        return r.charAt(0);
    });

    context.addMethod('debug', function (r, args) {
        console.log('debug: ', r);
        return r;
    });

    context.addMethod('prefixsp', function (r) {
        var result = joinArray(r, ", ");
        if (r.length > 0) {
            result.unshift(", ");
        }
        return result;
    });
    
    context.addMethod('join', function (r, args) {
        if (args.length == 0) {
            return joinArray(r, PP.NEWLINE);
        } else {
            return joinArray(r, args[0]);
        }
    });

    context.addMethod('njoin', function (r, args) {
        return joinArray(r, [PP.NEWLINE, PP.NEWLINE]);
    });

    context.addMethod('separator', function (r, args) {
        if (args.length == 0) {
            return joinArray(r, NEWLINE);
        } else {
            return joinArray(r, args[0]);
        }
    });
}

function Boolean(value) {
    this.value = value;
}

var False = new Boolean('FALSE');


exports.OP_EQL = OP_EQL;
exports.OP_NOT_EQL = OP_NOT_EQL;
exports.OP_HAS_KEY = OP_HAS_KEY;
exports.OP_ISA = OP_ISA;
exports.extend = extend;
exports.Context = Context;
exports.ASTAccess = ASTAccess;
exports.ASTPField = ASTPField;
exports.ASTPRecord = ASTPRecord;
exports.ASTPattern = ASTPattern;
exports.PolyMethod = PolyMethod;
exports.ASTMethod = ASTMethod;
exports.ASTString = ASTString;
exports.ASTNumber = ASTNumber;
exports.ASTCall = ASTCall;
exports.False = False;
exports.ASTEmpty = ASTEmpty;
exports.ASTCode = ASTCode;
exports.initDefaultMethods = initDefaultMethods;
exports.ASTUsing = ASTUsing;
exports.MethodInfo = MethodInfo;

