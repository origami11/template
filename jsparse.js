// Copyright (C) 2007 Chris Double.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// DEVELOPERS AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
// ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

// Вместо false информацию об ошибке!!!

function foldl(f, initial, seq) {
    for(var i=0; i< seq.length; ++i)
        initial = f(initial, seq[i]);
    return initial;
}

var memoize = true;

function ParseState(input, index) {
    this.input = input;
    this.index = index || 0;
    this.length = input.length - this.index;
    this.cache = { };
    return this;
}

ParseState.prototype.from = function(index) {
    var r = new ParseState(this.input, this.index + index);
    r.cache = this.cache;
    r.length = this.length - index;
    return r;
}

ParseState.prototype.substring = function(start, end) {
    return this.input.substring(start + this.index, (end || this.length) + this.index);
}

ParseState.prototype.trimLeft = function() {
    var s = this.substring(0);
    var m = s.match(/^\s+/);
    return m ? this.from(m[0].length) : this;
}

ParseState.prototype.at = function(index) {
    return this.input.charAt(this.index + index);
}

ParseState.prototype.toString = function() {
    return 'PS"' + this.substring(0) + '"';
}

ParseState.prototype.getCached = function(pid) {
    if(!memoize)
        return false;

    var p = this.cache[pid];
    if (p) {
        return p[this.index];
    } else {
        return false;
    }
}

ParseState.prototype.putCached = function(pid, cached) {
    if(!memoize)
        return;

    var p = this.cache[pid];
    if (p) {
        p[this.index] = cached;
    } else {
        p = this.cache[pid] = { };
        p[this.index] = cached;
    }
}


function ps(str) {
    return new ParseState(str);
}

// 'r' is the remaining string to be parsed.
// 'matched' is the portion of the string that
// was successfully matched by the parser.
// 'ast' is the AST returned by the successfull parse.
function Parsed(r, matched, ast, error) {
    return { remaining: r, matched: matched, ast: ast, result: true, error: error };
}

function NoParse(error) {
    return { result: false, error: error };
}

function Error(descriptor, position) {
    this.descriptors = descriptor;
    this.position = position;
}

Error.prototype.joinErrors = function(pe) {
    if (this.position > pe.position || pe.descriptors.length == 0) return this;
    if (this.position < pe.position || this.descriptors.length == 0) return pe;
    return new Error(this.descriptors.concat(pe.descriptors), this.position);
}

Error.prototype.getMessage = function (data, file) {
    var right = data.indexOf("\n", this.position);
    right = (right >= 0) ? right - 1: data.length;
    var left = data.lastIndexOf("\n", this.position);
    left = (left >= 0) ? left + 1 : 0;

    return this.descriptors.join(" or ") + " at " + (this.position + 1) + " in " + file + "\n" + 
        data.substring(left, right) + "\n" +
        require('./print.js').repeat(this.position - left) + "^";
}

function isParsed(r) {
    return r.result == true;
}

var parser_id = 0;

// 'token' is a parser combinator that given a string, returns a parser
// that parses that string value. The AST contains the string that was parsed.
function token(s) {
    var pid = parser_id++;
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached) {
            return cached;
        }

        var r = state.length >= s.length && state.substring(0, s.length) == s;
        if (r) {
            cached = Parsed(state.from(s.length), s, s, new Error([], state.index));
        } else {
            cached = NoParse(new Error(["expected " + s], state.index));
        }

        savedState.putCached(pid, cached);
        return cached;
    };
}


function rtoken(s, alias) {
    var pid = parser_id++;
    var rx = new RegExp("^(" + s + ")");
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached) {
            return cached;
        }

        var r = state.substring(0).match(rx);
        if (r) {
            cached = Parsed(state.from(r[0].length), r[1], r[1], new Error([], state.index));
        } else {
            cached = NoParse(new Error(["expected " + alias || s], state.index));
        }

        savedState.putCached(pid, cached);
        return cached;
    };
}


// Like 'token' but for a single character. Returns a parser that given a string
// containing a single character, parses that character value.
function ch(c) {
    var pid = parser_id++;
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached)
            return cached;

        var r = state.length >= 1 && state.at(0) == c;
        if (r) {
            cached = Parsed(state.from(1), c, c);
        } else {
            cached = NoParse();
        }

        savedState.putCached(pid, cached);
        return cached;
    };
}

// 'range' is a parser combinator that returns a single character parser
// (similar to 'ch'). It parses single characters that are in the inclusive
// range of the 'lower' and 'upper' bounds ("a" to "z" for example).
function range(lower, upper) {
    var pid = parser_id++;
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached)
            return cached;

        if(state.length < 1)
            cached = false;
        else {
            var ch = state.at(0);
            if(ch >= lower && ch <= upper)
                cached = Parsed(state.from(1), ch, ch);
            else
                cached = NoParse();
        }
        savedState.putCached(pid, cached);
        return cached;
    };
}


// Helper function to convert string literals to token parsers
// and perform other implicit parser conversions.
function toParser(p) {
    if (typeof(p) != 'function' && typeof(p) != "string") {
        throw "not a parser";
    }

    return (typeof(p) == "string") ? token(p) : p;
}

// Parser combinator that returns a parser that
// skips whitespace before applying parser.
function whitespace(p0) {
    var p = toParser(p0);
    var pid = parser_id++;
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached)
            return cached;

        cached = p(state.trimLeft());
        savedState.putCached(pid, cached);
        return cached;
    };
}


// Parser combinator that passes the AST generated from the parser 'p'
// to the function 'f'. The result of 'f' is used as the AST in the result.
function action(p0, f) {
    var p = toParser(p0);
    var pid = parser_id++;
    return function (state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached) {
            return cached;
        }

        var x = p(state);
        if (isParsed(x)) {
            x.ast = f(x.ast);
            cached = x;
        } else {
            cached = x;
        }
        savedState.putCached(pid, cached);
        return cached;
    };
}


// Given a parser that produces an array as an ast, returns a
// parser that produces an ast with the array joined by a separator.
function join_action(p, sep) {
    return action(p, function(ast) { return ast.join(sep); });
}

// Given an ast of the form [ Expression, [ a, b, ...] ], convert to
// [ [ [ Expression [ a ] ] b ] ... ]
// This is used for handling left recursive entries in the grammar. e.g.
// MemberExpression:
//   PrimaryExpression
//   FunctionExpression
//   MemberExpression [ Expression ]
//   MemberExpression . Identifier
//   new MemberExpression Arguments
function left_factor(ast) {
    return foldl(function(v, action) {
                     return action(v);
                 },
                 ast[0],
                 ast[1]);
}

// Return a parser that left factors the ast result of the original
// parser.
function left_factor_action(p) {
    return action(p, left_factor);
}

// 'negate' will negate a single character parser. So given 'ch("a")' it will successfully
// parse any character except for 'a'. Or 'negate(range("a", "z"))' will successfully parse
// anything except the lowercase characters a-z.
function negate(p0) {
    var p = toParser(p0);
    var pid = parser_id++;
    return function (state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached)
            return cached;

        if (state.length >= 1) {
            var r = p(state);
            if (!isParsed(r)) {
                cached =  Parsed(state.from(1), state.at(0), state.at(0), new Error([], state.index));
            } else {
                cached = NoParse(new Error(["unexpected error"], r.error.position));
            }
        } else {
            cached = NoParse(new Error(["unexpacted end of file"], state.index));
        }
        savedState.putCached(pid, cached);
        return cached;
    };
}



// 'end_p' is a parser that is successful if the input string is empty (ie. end of parse).
function end_p(state) {
    if(state.length == 0) {
        return Parsed(state, undefined, undefined, new Error([], state.index));
    } else {
        return NoParse(new Error(["unknown symbol"], state.index));
    }        
}


// 'nothing_p' is a parser that always fails.
function nothing_p(state) {
    return NoParse(new Error([], sate.index));
}

// 'sequence' is a parser combinator that processes a number of parsers in sequence.
// It can take any number of arguments, each one being a parser. The parser that 'sequence'
// returns succeeds if all the parsers in the sequence succeeds. It fails if any of them fail.
function sequence() {
    var parsers = [];
    for(var i = 0; i < arguments.length; ++i)
        parsers.push(toParser(arguments[i]));
    var pid = parser_id++;
    return function (state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached) {
            return cached;
        }

        var ast = [];
        var matched = "";
        var i;

        var error = new Error([], state.index);

        for (i = 0; i < parsers.length; ++i) {
            var parser = parsers[i];
            var result = parser(state);
            if (isParsed(result)) {
                state = result.remaining;
                if (result.ast != undefined) {
                    ast.push(result.ast);
                    matched = matched + result.matched;
                }
            } else {
                break;
            }
            error = error.joinErrors(result.error);
        }

        if (i == parsers.length) {
            cached = Parsed(state, matched, ast, error);
        } else {
            cached = NoParse(error.joinErrors(result.error));
        }

        savedState.putCached(pid, cached);
        return cached;
    };
}


// Like sequence, but ignores whitespace between individual parsers.
function wsequence() {
    var parsers = [];
    for(var i=0; i < arguments.length; ++i) {
        parsers.push(whitespace(toParser(arguments[i])));
    }
    return sequence.apply(null, parsers);
}


// 'choice' is a parser combinator that provides a choice between other parsers.
// It takes any number of parsers as arguments and returns a parser that will try
// each of the given parsers in order. The first one that succeeds results in a
// successfull parse. It fails if all parsers fail.
function choice() {
    var parsers = [];
    for (var i = 0; i < arguments.length; ++i) {
        parsers.push(toParser(arguments[i]));
    }        
    var pid = parser_id++;
    return function (state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached) {
            return cached;
        }
        var i;
        var error = new Error([], state.index);

        for (i = 0; i < parsers.length; ++i) {
            var parser = parsers[i];
            //
            var result = parser(state);
            error = error.joinErrors(result.error)
            if (isParsed(result)) {
                break;
            }
        }
        if (i == parsers.length) {
            cached = NoParse(error);
        } else {
            cached = result;
        }
        savedState.putCached(pid, cached);
        return cached;
    }
}


// 'butnot' is a parser combinator that takes two parsers, 'p1' and 'p2'.
// It returns a parser that succeeds if 'p1' matches and 'p2' does not, or
// 'p1' matches and the matched text is longer that p2's.
// Useful for things like: butnot(IdentifierName, ReservedWord)
function butnot(p01,p02) {
    var p1 = toParser(p01);
    var p2 = toParser(p02);
    var pid = parser_id++;

    // match a but not b. if both match and b's matched text is shorter
    // than a's, a failed match is made
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached) {
            return cached;
        }

        var br = p2(state);
        if(!isParsed(br)) {
            cached = p1(state);
        } else {
            var ar = p1(state);

            if (isParsed(ar)) {
                if(ar.matched.length > br.matched.length) {
                    cached = ar;
                } else {
                    cached = NoParse(ar.error);
                }  
            } else {
                cached = NoParse(ar.error);
            }
        }
        savedState.putCached(pid, cached);
        return cached;
    }
}


// 'difference' is a parser combinator that takes two parsers, 'p1' and 'p2'.
// It returns a parser that succeeds if 'p1' matches and 'p2' does not. If
// both match then if p2's matched text is shorter than p1's it is successfull.
function difference(p01,p02) {
    var p1 = toParser(p01);
    var p2 = toParser(p02);
    var pid = parser_id++;

    // match a but not b. if both match and b's matched text is shorter
    // than a's, a successfull match is made
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached)
            return cached;

        var br = p2(state);
        if(!isParsed(br)) {
            cached = p1(state);
        } else {
            var ar = p1(state);
            if(ar.matched.length >= br.matched.length)
                cached = br;
            else
                cached = ar;
        }
        savedState.putCached(pid, cached);
        return cached;
    }
}


// 'xor' is a parser combinator that takes two parsers, 'p1' and 'p2'.
// It returns a parser that succeeds if 'p1' or 'p2' match but fails if
// they both match.
function xor(p01, p02) {
    var p1 = toParser(p01);
    var p2 = toParser(p02);
    var pid = parser_id++;

    // match a or b but not both
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached)
            return cached;

        var ar = p1(state);
        var br = p2(state);
        if(isParsed(ar) && isParsed(br))
            cached = NoParse();
        else
            cached = isParsed(ar) ? ar : br;

        savedState.putCached(pid, cached);
        return cached;
    }
}

// A parser combinator that takes one parser. It returns a parser that
// looks for zero or more matches of the original parser.
function repeat0(p0) {
    var p = toParser(p0);
    var pid = parser_id++;

    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached) {
            return cached;
        }

        var ast = [];
        var matched = "";
        var result = p(state); 
        var error = result.error;        

        while(isParsed(result)) {

            ast.push(result.ast);
            matched = matched + result.matched;
            if (result.remaining.index == state.index) {
                break;
            }
            state = result.remaining;

            result = p(state);
            error = error.joinErrors(result.error);
        }
        cached = Parsed(state, matched, ast, error);
        savedState.putCached(pid, cached);
        return cached;
    }
}


// A parser combinator that takes one parser. It returns a parser that
// looks for one or more matches of the original parser.
function repeat1(p0) {
    var p = toParser(p0);
    var pid = parser_id++;

    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);

        if(cached) {
            return cached;
        }

        var ast = [];
        var matched = "";
        var result= p(state);
        if(!isParsed(result)) {
            cached = result; //NoParse();
        } else {
            var error = result.error;

            while(isParsed(result)) {
                ast.push(result.ast);
                matched = matched + result.matched;
                if(result.remaining.index == state.index)
                    break;
                state = result.remaining;
                result = p(state);

                error = error.joinErrors(result.error);
            }
            cached = Parsed(state, matched, ast, error);
        }
        savedState.putCached(pid, cached);
        return cached;
    }
}


// A parser combinator that takes one parser. It returns a parser that
// matches zero or one matches of the original parser.
function optional(p0) {
    var p = toParser(p0);
    var pid = parser_id++;
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached) {
            return cached;
        }
        var r = p(state);
        cached = isParsed(r) ? r : Parsed(state, "", false, new Error([], state.index));

        savedState.putCached(pid, cached);
        return cached;
    }
}


// A parser combinator that ensures that the given parser succeeds but
// ignores its result. This can be useful for parsing literals that you
// don't want to appear in the ast. eg:
// sequence(expect("("), Number, expect(")")) => ast: Number
function expect(p) {
    return action(p, function(ast) { return undefined; });
}

// p0 - parser
// s - parser (separator)
function chain(p0, s, f) {
    var p = toParser(p0);

    return action(sequence(p, repeat0(action(sequence(s, p), f))),
                  function(ast) { return [ast[0]].concat(ast[1]); });
}

// A parser combinator to do left chaining and evaluation. Like 'chain', it expects a parser
// for an item and for a seperator. The seperator parser's AST result should be a function
// of the form: function(lhs,rhs) { return x; }
// Where 'x' is the result of applying some operation to the lhs and rhs AST's from the item
// parser.
function chainl(p0, s) {
    var p = toParser(p0);
    return action(sequence(p, repeat0(sequence(s, p))),
                  function(ast) {
                      return foldl(function(v, action) { return action[0](v, action[1]); }, ast[0], ast[1]);
                  });
}

// A parser combinator that returns a parser that matches lists of things. The parser to
// match the list item and the parser to match the seperator need to
// be provided. The AST is the array of matched items.
function list(p, s) {
    return chain(p, s, function(ast) { return ast[1]; });
}

// Like list, but ignores whitespace between individual parsers.
function wlist() {
    var parsers = [];
    for(var i=0; i < arguments.length; ++i) {
        parsers.push(whitespace(arguments[i]));
    }
    return list.apply(null, parsers);
}

// A parser that always returns a zero length match
function epsilon_p(state) {
    return Parsed(state, "", undefined);
}

// Allows attaching of a function anywhere in the grammer. If the function returns
// true then parse succeeds otherwise it fails. Can be used for testing if a symbol
// is in the symbol table, etc.
function semantic(f) {
    var pid = parser_id++;
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached)
            return cached;
        cached = f() ? Parsed(state, "", undefined) : NoParse();
        savedState.putCached(pid, cached);
        return cached;
    }
}

// The and predicate asserts that a certain conditional
// syntax is satisfied before evaluating another production. Eg:
// sequence(and("0"), oct_p)
// (if a leading zero, then parse octal)
// It succeeds if 'p' succeeds and fails if 'p' fails. It never
// consume any input however, and doesn't put anything in the resulting
// AST.
function and(p0) {
    var p = toParser(p0);
    var pid = parser_id++;
    return function(state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if(cached)
            return cached;
        var r = p(state);
        cached = isParsed(r) ? Parsed(state, "", undefined) : NoParse();
        savedState.putCached(pid, cached);
        return cached;
    }
}

// The opposite of 'and'. It fails if 'p' succeeds and succeeds if
// 'p' fails. It never consumes any input. This combined with 'and' can
// be used for 'lookahead' and disambiguation of cases.
//
// Compare:
// sequence("a",choice("+","++"),"b")
//   parses a+b
//   but not a++b because the + matches the first part and peg's don't
//   backtrack to other choice options if they succeed but later things fail.
//
// sequence("a",choice(sequence("+", not("+")),"++"),"b")
//    parses a+b
//    parses a++b
//
function not(p0) {
    var p = toParser(p0);
    var pid = parser_id++;
    return function (state) {
        var savedState = state;
        var cached = savedState.getCached(pid);
        if (cached) {
            return cached;
        }
        cached = isParsed(p(state)) ? NoParse(new Error([], state.index)) : Parsed(state, "", undefined, new Error([], state.index));

        savedState.putCached(pid, cached);
        return cached;
    }
}


exports.ParseState = ParseState;
exports.token = token;
exports.rtoken = rtoken;
exports.range = range;
exports.whitespace = whitespace;
exports.action = action;
exports.negate = negate;
exports.end_p = end_p;
exports.sequence = sequence;
exports.wsequence = wsequence;
exports.choice = choice;
exports.butnot = butnot;
exports.repeat0 = repeat0;
exports.repeat1 = repeat1;
exports.optional = optional;
exports.not = not;
exports.chainl = chainl;
exports.chain = chain;
exports.wlist = wlist;
exports.left_factor_action = left_factor_action;
